![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![status-archived](https://bitbucket.org/repo/ekyaeEE/images/3278295154-status_archived.png)
![issues-closed](https://bitbucket.org/repo/ekyaeEE/images/1555006384-issues_closed.png)

# Rationale #

This repo describes some steps and paths to execute [Github desktop app](https://desktop.github.com/) inside a proxy environment.


### What is this repository for? ###

* Quick summary
     * Github desktop application running under a proxy server
     ![a1f56d02-3a5d-11e7-8799-23c7ba5e5106.png](https://bitbucket.org/repo/rp8ox5M/images/2788388409-a1f56d02-3a5d-11e7-8799-23c7ba5e5106.png)


### How do I get set up? ###

* Summary of set up
     * Mostly of the steps were done on MacOSX El Capitan environment. 
     * Update (2018): they were replicated on MacOSX Sierra with optimum result.
     * Update (2019): they were replicated on MacOSX High Sierra with best results.
     
* Configuration
     * Check the [issues](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/issues) 
* Dependencies
     * [Github desktop app](https://desktop.github.com/)
   	 * [Xcode comand line](https://developer.apple.com/download/more/)
	 * [GPG](https://www.gnupg.org/download/) (_optional_)
* How to run tests
     * Follow this [steps](/Github_desktop_app_installation.md)
* Deployment instructions
     * Check the [issues](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/issues)

### Related repositories

* [Setting up Github under proxy](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/master/)
* [Conferences](https://bitbucket.org/imhicihu/conferences/src/master/)
* [Streaming](https://bitbucket.org/imhicihu/streaming/src/master/)
* [Proxy settings (tutorials)](https://bitbucket.org/imhicihu/proxy-settings-tutorials/src/master/)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
     - Contact `imhicihu` at `gmail` dot `com`

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/master/code_of_conduct.md)
   
### Legal ###

* All trademarks are the property of their respective owners.

### Copyright ###
![88x31.png](https://bitbucket.org/repo/4pKrXRd/images/3902704043-88x31.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 2.0 Generic License](http://creativecommons.org/licenses/by-sa/2.0/).